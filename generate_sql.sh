openapi-generator-cli generate -g mysql-schema -o schema -i openapi/inventory-api.yaml
# Insert used database at top of generated schema
sed -i '1s/^/USE neoinventory_db;\n/' ./schema/mysql_schema.sql
