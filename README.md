# NeoInventory

## Installation

First, install java on the device

In this folder:
```
python -m venv python_venv
source python_venv/bin/activate
pip install -r requirements.txt
nodeenv node_venv
source node_venv/bin/activate
npm install @openapitools/openapi-generator-cli -g
openapi-generator-cli version-manager set 7.0.0
source generate_sql.sh
```

## Run mysql database
```
docker-compose up
```
In a broser, open http://localhost:8082 to and log in. Choose neoinventory_db and Components. Add an item to Components for testing.

## Setting up API
```
python components.py
```

## Test using GUI
In a browser, open `http://localhost:9090/ui`

## Test using CURL
If everything seems error free after this point, call the following curl to get from the API:
```
 curl -X 'GET'   'http://localhost:9090/component'   -H 'accept: */*'
```

## Useful links:
To edit OpenAPI: https://editor.swagger.io/
Or if GUI is preffered for editing: https://mermade.github.io/openapi-gui/
