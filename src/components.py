#!/usr/bin/env python3

import connexion
from sqlalchemy import *
from flask import request

class Database:
    def __init__(self,init_engine,echo):
        self.engine = create_engine(init_engine,echo=echo)
        self.metadata_obj = MetaData()
        self.inspection = inspect(self.engine)
        self.table_names = self.inspection.get_table_names()
        if not "Component" in self.table_names:
            print("ERROR")
            exit(1)

# Global database object
db = Database(init_engine="mysql+pymysql://root:medlem@localhost:3306/neoinventory_db",echo=False)

def get_schema(schema,search_query):
    components = list()
    if not schema in db.table_names:
        return ("Not a valid schema", 404)
    table = Table(
            schema,
            db.metadata_obj,
            autoload_with=db.engine
    )
    # stmt is short for statement
    stmt = select(table).filter_by(**search_query)
    with db.engine.connect() as conn:
        result = conn.execute(stmt)
        for row in result:
            components.append(row._asdict())
    return components

def add_item(schema,item):
    if not schema in db.table_names:
        return ("Not a valid schema", 404)
    table = Table(
            schema,
            db.metadata_obj,
            autoload_with=db.engine
    )
    new_id = item['id']
    stmt = select(table).where(table.c.id == new_id)
    with db.engine.connect() as conn:
        result = conn.execute(stmt)
        if(result.rowcount > 0):
            return ("Item ID not unique", 403)
            
    stmt = insert(table).values(item)
    with db.engine.connect() as conn:
        conn.execute(stmt)
        conn.commit()
    return ("Add success",200)
