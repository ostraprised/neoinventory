from pathlib import Path
import connexion

app = connexion.AsyncApp(__name__, specification_dir='../openapi/')
app.add_api('inventory-api.yaml', arguments={'title': 'Hello World Example'}, validate_responses=True)

if __name__ == '__main__':
    app.run(f"{Path(__file__).stem}:app",port=9090)
